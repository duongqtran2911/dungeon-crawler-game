# Meeting 10.12.2021 16:00

**Participants**:
1. Bach Xuan Bui
2. Duong Tran
3. Joel Häkkinen
4. Tino Saarinen


## Summary of works

1. Bach:
    - Basic pause state done.






2. Duong:
    - Worked on the Documentation.
    - Implemented the basis of Creatures classes.




3. Joel:
    - Created two different types of enemies.



4. Tino:
    - 


## Challenges

1. Bach: Multiple pressed button is still a problem.
2. Duong: Integrate the Creatures classes into the game.
3. Joel: None
4. Tino: 



## Actions
1. Bach: Fix the problem as soon as possible.
2. Duong: Continue the documentation and keep editing the project.
3. Joel: None
4. Tino: 



## Project status
The project is basically finished. Only a few tweaks here and there



## TODOs
1. Bach: 
    - Finish the game over state.



2. Duong:
    - Documentation.
    - Try to implement another additional feature.


3. Joel:
    - Unify the coding style so that it will follow the Google C++ style guide.
    - Writing the final documentation.
    - Study sounds in SFML.


4. Tino:
    - 


## Deviations and changes
    - No important changes. We will implement some additional feature if there are enough time.


